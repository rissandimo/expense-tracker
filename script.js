// dom elements
const balanceDOM = document.getElementById('balance');
const moneyPlus = document.getElementById('money-plus');
const moneyMinus = document.getElementById('money-minus');
const expenseHistoryList = document.getElementById('list');
const form = document.getElementById('form');
const description = document.getElementById('description');
const amount = document.getElementById('amount');

    //get transactions and convert to JSON
    const localStorageTransactions = JSON.parse(
        localStorage.getItem('transactions')
      );

    //if there are existing transactions - assign the local storage transactions; otherwise assign an empty array
    let transactions = localStorage.getItem('transactions') !== null ? localStorageTransactions : [];

function addNewTransaction(event)
{
    event.preventDefault();

    //check if input entered
    if(description.value.trim() === '' || amount.value.trim() === ''){
        alert("Please add a description and amount");
        return;
    }

    const newTransaction = {
        id: generateRandomNumber(),
        description: description.value,
        amount: parseInt(amount.value)
    };     

    //save transactions to array
    transactions.push(newTransaction);

    addTransctionToDom(newTransaction);

    calculateBalance();

    updateLocalStorage();

    clearForm();
    
}

function addTransctionToDom(newTransaction)
{    
    //get sign
    const sign = newTransaction.amount < 0 ? '-' : '+';    

    //create list element
    const transaction = document.createElement('li');

    //add class
    transaction.classList.add(newTransaction.amount < 0 ? 'minus' : 'plus');
    //up to here works

    //add description & amount
    transaction.innerHTML = `
        ${newTransaction.description} <span>${sign} ${Math.abs(newTransaction.amount)}</span>
        <button class="delete-btn" onclick="removeTransaction(${newTransaction.id})">x</button></li>
    `;    

    //append transaction to expense history
    expenseHistoryList.appendChild(transaction);

    calculateBalance();
}

function calculateBalance()
{
    
    const allTransactionAmounts = transactions.map(transaction => transaction.amount);

    const balance = allTransactionAmounts.reduce( (accum, amount) => (accum += amount), 0).toFixed(2);

    const incomes = calculateIncomes(allTransactionAmounts);                                      
    
    const expenses = calculateExpenses(allTransactionAmounts);    

    balanceDOM.innerHTML = `$${balance}`;
    moneyPlus.innerHTML = `$${incomes}`;
    moneyMinus.innerHTML = `$${expenses}`;
}


function calculateIncomes(expenseHistory){
    const totalIncome = expenseHistory.filter(amount => amount > 0)
                  .reduce( (accum, amount) => (accum += amount), 0).toFixed(2);
    return totalIncome;
}

function calculateExpenses(expenseHistory){
   const totalExpense = (expenseHistory.filter(amount => amount < 0)
                    .reduce( (accum, amount) => (accum += amount) , 0) * -1).toFixed(2);
    return totalExpense;
}

function clearForm(){
    description.value = '';
    amount.value = '';
}

function generateRandomNumber(){
    return Math.floor(Math.random() * 10000000);
}

function init(){
    //clear the expense history
    expenseHistoryList.innerHTML = '';

        transactions.forEach(addTransctionToDom);
        calculateBalance();
}

function removeTransaction(idToDelete){
    transactions = transactions.filter(transaction => transaction.id != idToDelete);

    updateLocalStorage();

    init();     
}

function updateLocalStorage(){
    //save array to local storage
    localStorage.setItem('transactions', JSON.stringify(transactions));
}

//event handlers

form.addEventListener('submit', addNewTransaction);

init();